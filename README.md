# README #

### What is this repository for? ###

* TradeMe Test

### How do I get set up? ###

* Clone the project into local folder
* Open TradeMeDemo.xcworkspace in Xcode
* Run in Simulator or Device (additional steps of setting signing account needed)
* 'pod install' is not required as Pod folder is committed

### Who do I talk to? ###

* ZHANG Chenglong(Felix)
* Email: chenglong.zhang@outlook.com