//
//  ListingCell.swift
//  TradeMeDemo
//
//  Created by ZHANG Chenglong on 20/08/17.
//  Copyright © 2017 ZHANG Chenglong. All rights reserved.
//

import UIKit
import SDWebImage

class ListingCell: UITableViewCell {
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var dateFormatter: DateFormatter!
    
    var listing: Listing? {
        didSet {
            if let imageUrl = listing?.pictureHref {
                self.thumbnailImageView.sd_setImage(with: URL(string: imageUrl), completed: nil)
            }
            self.titleLabel.text = listing?.title
            self.locationLabel.text = listing?.region
            if let date = listing?.startDate {
                self.dateLabel.text = dateFormatter.string(from: date)
            }
            self.priceLabel.text = listing?.priceDisplay
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        self.thumbnailImageView.image = nil
        self.titleLabel.text = "Title"
        self.locationLabel.text = "Location"
        self.dateLabel.text = "Date"
        self.priceLabel.text = "Price"
    }

}
