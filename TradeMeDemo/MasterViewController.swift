//
//  MasterViewController.swift
//  TradeMeDemo
//
//  Created by ZHANG Chenglong on 19/08/17.
//  Copyright © 2017 ZHANG Chenglong. All rights reserved.
//

import UIKit

protocol MasterViewControllerDelegate: class {

}

class MasterViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var showButtonHeightConstraint: NSLayoutConstraint!
    
    weak var delegate: MasterViewControllerDelegate?
    var category: TMCategory?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.DETAILVIEWCONTROLLER_RECREATE_NOTIFICATION), object: nil, queue: nil) {[weak self] (notification) in
            self?.delegate = notification.object as? MasterViewControllerDelegate
        }
        
        // Set splitViewController's delegate in viewDidLoad to guarantee show masterViewController when launch on iPhone
        splitViewController?.delegate = self
        
        // Construct a root category when category is nil
        if self.category == nil {
            self.category = TMCategory(JSON: [
                "Name": "Root",
                "Number": "0",
                "CanHaveSecondCategory": true
                ])
        }
        
        self.fetchCategory()
        
        self.adjustHeightShowListingsButton(to: UIScreen.main.bounds.size)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Set splitViewController's delegate in viewWillAppear to guarantee current masterViewController will response splitViewController's delegate methods
        splitViewController?.delegate = self
        
        if self.category != nil {
            // Load listings on detailViewController
            if let detailViewController = self.delegate as? DetailViewController {
                detailViewController.category = self.category
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchCategory() {
        self.serviceProvider.fetchSubCategories(withCategory: self.category) { (result) in
            switch result {
            case .success(let value):
                self.category?.subcategories = value
                self.tableView.reloadData()
            case .failure(_):
                self.showErrorView()
            }
            self.hideHUD()
        }
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (context) in
            self.adjustHeightShowListingsButton(to: size)
        }) { (context) in
            
        }
    }
    
    func adjustHeightShowListingsButton(to size: CGSize) {
        // Only valid if it is iPhone plus
        if UIScreen.main.nativeBounds.height == 2208 {
            // Show "show listings" button in portrait
            if size.height == 736 {
                self.showButtonHeightConstraint.constant = 50
            } else {
                // Hide "show listings" button in landscape
                self.showButtonHeightConstraint.constant = 0
            }
        } else if UIDevice.current.userInterfaceIdiom == .pad {
            // Hide "show listings" button on iPad
            self.showButtonHeightConstraint.constant = 0
        }
    }

    @IBAction func didPressShowListing(_ sender: UIButton) {
        if let detailViewController = self.delegate as? DetailViewController {
            self.splitViewController?.showDetailViewController(detailViewController.navigationController!, sender: nil)
        }
    }
}

extension MasterViewController: UISplitViewControllerDelegate {
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        // Return true to indicate collapse is handled, secondary view controller will be released
        return true
    }
    
    func splitViewController(_ splitViewController: UISplitViewController, separateSecondaryFrom primaryViewController: UIViewController) -> UIViewController? {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detailNavigationController = storyboard.instantiateViewController(withIdentifier: "DetailNavigationController") as! UINavigationController
        let detailViewController = detailNavigationController.topViewController as! DetailViewController
        detailViewController.category = self.category
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constant.DETAILVIEWCONTROLLER_RECREATE_NOTIFICATION), object: detailViewController)
        return detailNavigationController
    }
}

extension MasterViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let categoryCount = self.category?.subcategories?.count else {
            return 0
        }
        return categoryCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        guard let categories = self.category?.subcategories else {
            cell.textLabel?.text = "Undefined"
            return cell
        }
        let category = categories[indexPath.row]
        cell.textLabel?.text = category.name
        cell.accessoryType = .none
        if let subCategories = category.subcategories {
            if subCategories.count > 0 {
                cell.accessoryType = .disclosureIndicator
            }
        }
        return cell
    }
}

extension MasterViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let categories = self.category?.subcategories else {
            return
        }
        let category = categories[indexPath.row]
        if category.subcategories == nil || category.subcategories!.count == 0 {
            if let detailViewController = self.delegate as? DetailViewController {
                self.splitViewController?.showDetailViewController(detailViewController.navigationController!, sender: nil)
            }
            return
        }
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let subMasterViewController = storyBoard.instantiateViewController(withIdentifier: "MasterViewController") as! MasterViewController
        subMasterViewController.delegate = self.delegate
        subMasterViewController.serviceProvider = ServiceProvider.sharedInstance
        subMasterViewController.category = category
        self.navigationController?.pushViewController(subMasterViewController, animated: true)
    }
}
