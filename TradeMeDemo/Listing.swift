//
//  Listing.swift
//  TradeMeDemo
//
//  Created by ZHANG Chenglong on 19/08/17.
//  Copyright © 2017 ZHANG Chenglong. All rights reserved.
//

import ObjectMapper

class Listing: Mappable {
    var listingId = 0
    var title = ""
    var category = ""
    var region = ""
    var startDate: Date?
    var pictureHref: String?
    var priceDisplay = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        listingId <- map["ListingId"]
        title <- map["Title"]
        category <- map["Category"]
        region <- map["Region"]
        startDate <- (map["StartDate"], TMDateTransform())
        pictureHref <- map["PictureHref"]
        priceDisplay <- map["PriceDisplay"]
    }
}

class TMDateTransform: DateTransform {
    override func transformFromJSON(_ value: Any?) -> Date? {
        if let timeInt = value as? Double {
            return Date(timeIntervalSince1970: TimeInterval(timeInt))
        }
        
        if let timeStr = value as? String {
            // Get new time string which remove /Date()/ and last 3 digits
            let start = timeStr.index(timeStr.startIndex, offsetBy: 6)
            let end = timeStr.index(timeStr.endIndex, offsetBy: -5)
            let range = start..<end
            let newTimeStr = timeStr.substring(with: range)
            return Date(timeIntervalSince1970: TimeInterval(atof(newTimeStr)))
        }
        
        return nil
    }
}
