//
//  TMCategory.swift
//  TradeMeDemo
//
//  Created by ZHANG Chenglong on 19/08/17.
//  Copyright © 2017 ZHANG Chenglong. All rights reserved.
//

import ObjectMapper

class TMCategory: Mappable {
    var name = ""
    var number = ""
    var path = ""
    var subcategories: [TMCategory]?
    var listings: [Listing]?
    var canHaveSecondCategory = false
    
    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        name <- map["Name"]
        number <- map["Number"]
        path <- map["Path"]
        subcategories <- map["Subcategories"]
        canHaveSecondCategory <- map["CanHaveSecondCategory"]
    }
    
}
