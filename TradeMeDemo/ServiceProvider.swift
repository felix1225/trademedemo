//
//  ServiceProvider.swift
//  TradeMeDemo
//
//  Created by ZHANG Chenglong on 19/08/17.
//  Copyright © 2017 ZHANG Chenglong. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper

struct ServerConfig {
    #if DEBUG
    public static let BaseUrl = "https://api.tmsandbox.co.nz/v1/"
    #else
    public static let BaseUrl = "https://api.trademe.co.nz/v1/"
    #endif
    public static let CategorySegment = "Categories/"
    public static let GeneralSearchSegment = "Search/General"
    public static let ListingDetailSegment = "Listings/"
    public static let Suffix = ".json"
    
    public static let SearchResultNumber = 20
    
    public static let OAuthConsumerKey = "A1AC63F0332A131A78FAC304D007E7D1"
    public static let OAuthConsumerSecret = "EC7F18B17A062962C6930A8AE88B16C7"
    public static let OAuthSignatureMethod = "PLAINTEXT"
}

class ServiceProvider: ServiceProviderProtocol {
    static let sharedInstance = ServiceProvider()
    
    /// Request subcategories using depth 2
    ///
    /// - Parameters:
    ///   - category: current category, request root category's subcategories when nil
    ///   - completion: callback after completion
    func fetchSubCategories(withCategory category: TMCategory?, completion: @escaping (Result<[TMCategory]>) -> Void) {
        var url = ServerConfig.BaseUrl + ServerConfig.CategorySegment
        url += ((category != nil) ? category!.number : "0")
        url += ServerConfig.Suffix
        let params = [
            "depth": 2
        ]
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseArray(queue: nil, keyPath: "Subcategories", context: nil) { (response: DataResponse<[TMCategory]>) in
            completion(response.result)
        }
    }
    
    /// Construct authorization header of OAuth 1.0a
    ///
    /// - Returns: authorization header
    func constructHeader() -> [String: String] {
        return [
            "Authorization": "OAuth oauth_consumer_key=\"\(ServerConfig.OAuthConsumerKey)\",oauth_signature_method=\"\(ServerConfig.OAuthSignatureMethod)\",oauth_timestamp=\"\(String(Int(Date().timeIntervalSince1970)))\",oauth_nonce=\"\(self.randomString(length: 11))\",oauth_version=\"1.0\",oauth_signature=\"\(ServerConfig.OAuthConsumerSecret)%26\""
        ]
    }
    
    func randomString(length: Int) -> String {
        let letters: NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    func fetchListing(withCategory category: TMCategory, completion: @escaping (Result<[Listing]>) -> Void) {
        let url = ServerConfig.BaseUrl + ServerConfig.GeneralSearchSegment + ServerConfig.Suffix
        let params = [
            "category": category.number,
            "rows": ServerConfig.SearchResultNumber
        ] as [String : Any]
        let headers = self.constructHeader()
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).responseArray(queue: nil, keyPath: "List", context: nil) { (response: DataResponse<[Listing]>) in
            completion(response.result)
        }
    }
    
    func fetchListingDetail(withListing listing: Listing, completion: @escaping (Result<ListingDetail>) -> Void) {
        let url = ServerConfig.BaseUrl + ServerConfig.ListingDetailSegment + String(describing: listing.listingId) + ServerConfig.Suffix
        let params = [
            "increment_view_count": false,
            "question_limit": 0,
            "return_member_profile": false
            ] as [String : Any]
        let headers = self.constructHeader()
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<ListingDetail>) in
            completion(response.result)
        }
    }

}
