//
//  Photo.swift
//  TradeMeDemo
//
//  Created by ZHANG Chenglong on 22/08/17.
//  Copyright © 2017 ZHANG Chenglong. All rights reserved.
//

import ObjectMapper

class Photo: Mappable {
    var photoId = 0
    var value: String?

    required /// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
    init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        photoId <- map["Key"]
        value <- map["Value.Medium"]
    }
}
