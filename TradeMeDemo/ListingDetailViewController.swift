//
//  ListingDetailViewController.swift
//  TradeMeDemo
//
//  Created by ZHANG Chenglong on 22/08/17.
//  Copyright © 2017 ZHANG Chenglong. All rights reserved.
//

import UIKit

class ListingDetailViewController: BaseViewController {

    @IBOutlet weak var listingIDLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    var listing: Listing?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        serviceProvider = ServiceProvider.sharedInstance
        self.fetchListingDetail()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchListingDetail() {
        guard let listing = self.listing else {
            return
        }
        
        serviceProvider.fetchListingDetail(withListing: listing) { (result) in
            switch result {
            case .success(let value):
                self.listingIDLabel.text = String(describing: value.listingId)
                self.titleLabel.text = value.title
                if let photo = value.photo {
                    self.thumbnailImageView.sd_setImage(with: URL(string: photo), completed: nil)
                }
            case .failure(_):
                self.showErrorView()
            }
            self.hideHUD()
        }
    }

}
