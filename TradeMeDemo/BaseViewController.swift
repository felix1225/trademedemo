//
//  BaseViewController.swift
//  TradeMeDemo
//
//  Created by ZHANG Chenglong on 22/08/17.
//  Copyright © 2017 ZHANG Chenglong. All rights reserved.
//

import UIKit
import MBProgressHUD

class BaseViewController: UIViewController {
    
    var serviceProvider: ServiceProviderProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    // TODO: show error view
    func showErrorView() {
        
    }
}
