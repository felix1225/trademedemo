//
//  DetailViewController.swift
//  TradeMeDemo
//
//  Created by ZHANG Chenglong on 19/08/17.
//  Copyright © 2017 ZHANG Chenglong. All rights reserved.
//

import UIKit

class DetailViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    
    var category: TMCategory! {
        didSet {
            if category.number == "0" {
                self.view.bringSubview(toFront: self.emptyView)
            } else {
                self.view.sendSubview(toBack: self.emptyView)
                self.fetchListing()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        serviceProvider = ServiceProvider.sharedInstance
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchListing() {
        self.serviceProvider.fetchListing(withCategory: self.category) { (result) in
            switch result {
            case .success(let value):
                self.category.listings = value
                self.tableView.reloadData()
            case .failure(_):
                self.showErrorView()
            }
            self.hideHUD()
        }
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "listingDetail":
            let destViewController = segue.destination as! ListingDetailViewController
            destViewController.listing = sender as? Listing
        default:
            break
        }
    }

}

extension DetailViewController: MasterViewControllerDelegate {

}

extension DetailViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.category?.listings?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListingCell", for: indexPath) as! ListingCell
        guard let listings = self.category.listings else { return cell }
        cell.listing = listings[indexPath.row]
        
        return cell
    }
}

extension DetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let listings = self.category.listings {
            performSegue(withIdentifier: "listingDetail", sender: listings[indexPath.row])
        }
    }
}
