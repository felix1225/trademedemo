//
//  CALayer+XibConfiguration.swift
//  TradeMeDemo
//
//  Created by ZHANG Chenglong on 21/08/17.
//  Copyright © 2017 ZHANG Chenglong. All rights reserved.
//

import UIKit

extension CALayer {
    var borderUIColor: UIColor {
        set {
            self.borderColor = newValue.cgColor
        }
        
        get {
            return UIColor(cgColor: self.borderColor!)
        }
    }
}
