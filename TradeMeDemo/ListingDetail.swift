//
//  ListingDetail.swift
//  TradeMeDemo
//
//  Created by ZHANG Chenglong on 22/08/17.
//  Copyright © 2017 ZHANG Chenglong. All rights reserved.
//

import ObjectMapper

class ListingDetail: Mappable {
    var listingId = 0
    var title = ""
    var photoId = 0
    var photos: [Photo]?
    var photo: String? {
        get {
            if self.photos != nil && self.photos!.count > 0 {
                let filterPhotos = self.photos!.filter({ (photo) -> Bool in
                    photo.photoId == self.photoId
                })
                if filterPhotos.count > 0 {
                    return filterPhotos[0].value
                }
            }
            return nil
        }
    }
    
    required /// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
    init?(map: Map) {
        
    }

    func mapping(map: Map) {
        listingId <- map["ListingId"]
        title <- map["Title"]
        photoId <- map["PhotoId"]
        photos <- map["Photos"]
    }
}
