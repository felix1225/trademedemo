//
//  ServiceProviderProtocol.swift
//  TradeMeDemo
//
//  Created by ZHANG Chenglong on 21/08/17.
//  Copyright © 2017 ZHANG Chenglong. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper

protocol ServiceProviderProtocol {
    func fetchSubCategories(withCategory category: TMCategory?, completion: @escaping (Result<[TMCategory]>) -> Void)
    func fetchListing(withCategory category: TMCategory, completion: @escaping (Result<[Listing]>) -> Void)
    func fetchListingDetail(withListing listing: Listing, completion: @escaping (Result<ListingDetail>) -> Void)
}
