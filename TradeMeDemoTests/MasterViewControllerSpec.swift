//
//  MasterViewControllerSpec.swift
//  TradeMeDemo
//
//  Created by ZHANG Chenglong on 21/08/17.
//  Copyright © 2017 ZHANG Chenglong. All rights reserved.
//

import Quick
import Nimble
import Alamofire
@testable import TradeMeDemo

class MasterViewControllerSpec: QuickSpec {
    override func spec() {
        var viewController: MasterViewController!
        
        beforeEach {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            viewController = storyboard.instantiateViewController(withIdentifier: "MasterViewController") as! MasterViewController
        }
        
        describe("Init MasterViewController") {
            beforeEach {
                viewController.serviceProvider = MockServiceProvider()
            }
            
            afterEach {
                viewController.serviceProvider = nil
            }
            
            it("sets category as nil", closure: {
                expect(viewController.category).to(beNil())
            })
            
            it("has not called fetchSubCategories", closure: {
                let provider = viewController.serviceProvider as! MockServiceProvider
                expect(provider.fetchSubCategoriesCalled).to(beFalse())
            })
            
            describe(".viewDidLoad", { 
                beforeEach {
                    let _ = viewController.view
                }
                
                it("sets category as not nil", closure: { 
                    expect(viewController.category).toNot(beNil())
                })
                
                it("calls fetchSubCategories", closure: {
                    let provider = viewController.serviceProvider as! MockServiceProvider
                    expect(provider.fetchSubCategoriesCalled).to(beTrue())
                })
            })
        }
    }
}

class MockServiceProvider: ServiceProviderProtocol {
    var fetchSubCategoriesCalled = false, fetchListingCalled = false
    
    func fetchSubCategories(withCategory category: TMCategory?, completion: @escaping (Result<[TMCategory]>) -> Void) {
        fetchSubCategoriesCalled = true
    }
    
    func fetchListing(withCategory category: TMCategory, completion: @escaping (Result<[Listing]>) -> Void) {
        fetchListingCalled = true
    }
}
