//
//  ServiceProviderSpec.swift
//  TradeMeDemo
//
//  Created by ZHANG Chenglong on 21/08/17.
//  Copyright © 2017 ZHANG Chenglong. All rights reserved.
//

import Quick
import Nimble
import Alamofire
import ObjectMapper
@testable import TradeMeDemo

class ServiceProviderSpec: QuickSpec {
    override func spec() {
        var serviceProvider: ServiceProvider!
        var category: TMCategory!
        
        beforeEach {
            serviceProvider = ServiceProvider.sharedInstance
            category = TMCategory(JSON: [
                "Name": "Root",
                "Number": "0",
                "CanHaveSecondCategory": true
                ])
        }
        
        afterEach {
            serviceProvider = nil
            category = nil
        }
        
        describe("serviceProvider") {
            it("is not nil", closure: {
                expect(serviceProvider).toNot(beNil())
            })
        }
        
        describe("fetchSubCategories") {
            var returnedCategories: [TMCategory]?
            
            it("returns categories", closure: {
                serviceProvider.fetchSubCategories(withCategory: category, completion: { (result) in
                    returnedCategories = result.value
                })
                
                expect(returnedCategories).toEventuallyNot(beNil(), timeout: 5)
            })
        }
        
        describe("fetchListing") {
            var returnedListings: [Listing]?
            
            it("returns categories", closure: {
                serviceProvider.fetchListing(withCategory: category, completion: { (result) in
                    returnedListings = result.value
                })
                
                expect(returnedListings).toEventuallyNot(beNil(), timeout: 5)
            })
        }
    }
}
